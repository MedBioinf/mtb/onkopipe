# Reference file download

This is a instruction for reference files download.

## Authors

* Jingyu Yang


## Usage


#### 1: Download following GDC genome reference data on this folder or your own `[ref file path]`

1. [Reference genome(FASTQ)](https://api.gdc.cancer.gov/data/254f697d-310d-4d7d-a27b-27fbf767a834).
2. [BWA Index](https://api.gdc.cancer.gov/data/25217ec9-af07-4a17-8db9-101271ee7225).
3. [GATK Index](https://api.gdc.cancer.gov/data/2c5730fb-0909-4e2a-8a7a-c9a7f8b2dad5)

#### 2: Download following GATK reference data on this folder or your own `[ref file path]`

1. know_dbsnp_vcf ([dbsnp_146.hg38.vcf.gz](wget -c ftp://gsapubftp-anonymous@ftp.broadinstitute.org/bundle/hg38/dbsnp_146.hg38.vcf.gz), [dbsnp_146.hg38.vcf.gz.tbi](wget -c ftp://gsapubftp-anonymous@ftp.broadinstitute.org/bundle/hg38/dbsnp_146.hg38.vcf.gz.tbi))
2. know_dbsnp_vcf ([Homo_sapiens_assembly38.known_indels.vcf.gz](https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.known_indels.vcf.gz), [Homo_sapiens_assembly38.known_indels.vcf.gz.tbi](https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.known_indels.vcf.gz.tbi))
3. gold_standard_indels ([Mills_and_1000G_gold_standard.indels.hg38.vcf.gz](https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz), [Mills_and_1000G_gold_standard.indels.hg38.vcf.gz.tbi](https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz.tbi))
4. 1000G_snpshigh_confidence ([1000G_phase1.snps.high_confidence.hg38.vcf.gz](https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/1000G_phase1.snps.high_confidence.hg38.vcf.gz), [1000G_phase1.snps.high_confidence.hg38.vcf.gz.tbi](https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/1000G_phase1.snps.high_confidence.hg38.vcf.gz.tbi))
5. interval_list ([wgs_calling_regions.hg38.interval_list](https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/wgs_calling_regions.hg38.interval_list))
6. af_only_gnomad ([af-only-gnomad.hg38.vcf.gz](https://storage.googleapis.com/gatk-best-practices/somatic-hg38/af-only-gnomad.hg38.vcf.gz), [af-only-gnomad.hg38.vcf.gz.tbi](https://storage.googleapis.com/gatk-best-practices/somatic-hg38/af-only-gnomad.hg38.vcf.gz.tbi))
7. gatk_panel_of_normal ([1000g_pon.hg38.vcf.gz](https://storage.googleapis.com/gatk-best-practices/somatic-hg38/1000g_pon.hg38.vcf.gz), [1000g_pon.hg38.vcf.gz.tbi](https://storage.googleapis.com/gatk-best-practices/somatic-hg38/1000g_pon.hg38.vcf.gz.tbi))
8. exac_common_knownsite ([small_exac_common_3.hg38.vcf.gz](https://storage.googleapis.com/gatk-best-practices/somatic-hg38/small_exac_common_3.hg38.vcf.gz), [small_exac_common_3.hg38.vcf.gz.tbi](https://storage.googleapis.com/gatk-best-practices/somatic-hg38/small_exac_common_3.hg38.vcf.gz.tbi))

